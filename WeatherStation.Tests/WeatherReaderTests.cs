using _2023ReworkOODLesson.End.Loggers;
using _2023ReworkOODLesson.End.Readers;
using _2023ReworkOODLesson.End.Stations;
using _2023ReworkOODLesson.Start;
using WeatherStation.Tests.Mocks;

namespace Weather.Tests
{
    /// <summary>
    /// This class exists to show the difference in testing our weather station before and after its been cleaned up.
    /// <para>Even though the first test looks shorter with no faked dependencies, you need to do setup external to the tests. This is not a deterministic way of testing, and can cause more issues. Faking dependencies is the proper way to do it as you have full control.</para>
    /// </summary>
    public class WeatherReaderTests
    {
        // If we want to test proper values, we need to have a file that exists and contains the data we are expecting.
        // This is much more difficult to do if we have a database instead of a csv file for example.
        // We also arent properly unit testing, as we are involving multiple units, and relying on a real file that is external to our applicaiton.

        [Fact]
        public void ReadWeatherData_FileExists_ShouldReturnProperValues()
        {
            // Arrange
            string name = "TestStation";
            string path = "data.csv";
            _2023ReworkOODLesson.Start.WeatherStation station = new _2023ReworkOODLesson.Start.WeatherStation(name);
            // An external file with these values needs to exist for this test to pass. We may not have control over that, so its not garenteed we are being deterministic.
            List<WeatherData> expected = new List<WeatherData>() { new WeatherData() { Date = DateTime.Today, Temperature = 10, Humidity = 50, Pressure = 1000 } };
            // Act
            station.ReadWeatherData(path);
            List<WeatherData> actual = station.Readings;
            // Assert
            Assert.Equal(expected, actual);
        }

        // Now because we have made our classes testable, we can simply mock the reader dependency.
        // We no longer have to rely on something we cannot control (the file existing and being populated with the data we need)
        // Now our tests are much more deterministic and reliable.

        [Fact]
        public void ReadWeatherData_MockData_ShouldReturnProperValues()
        {
            // Arrange
            string name = "TestStation";
            string path = ""; // Path doesnt matter here because our mock will always return the same data. We can make a separate test using a real dependency to test what happens if a file does not exist.
            string address = "TestAddress";
            IWeatherDataReader reader = new MockWeatherDataReader(); // Mocked dependency
            IWeatherDataLogger logger = new WeatherDataConsoleLogger(); // Real dependency, we dont need to mock it for this test. Or other tests, as its just printing to the console. Testing this functionality will require a different solution.
            _2023ReworkOODLesson.End.Stations.WeatherStation station = new LocalWeatherStation(name, address, reader, logger);
            List<_2023ReworkOODLesson.End.Data.WeatherData> expected = new List<_2023ReworkOODLesson.End.Data.WeatherData>() { new _2023ReworkOODLesson.End.Data.WeatherData() { Date = DateTime.Today, Temperature = 10, Humidity = 50, Pressure = 1000 } }; // We can force the reader to return this. This tests that the values are being assigned correctly in our WeatherStation class.
            // Act
            station.ReadWeatherData(path);
            var actual = station.Readings;
            // Assert
            Assert.Equal(expected, actual);
        }
    }
}