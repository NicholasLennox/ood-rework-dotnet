﻿using _2023ReworkOODLesson.End.Data;
using _2023ReworkOODLesson.End.Readers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherStation.Tests.Mocks
{
    /// <summary>
    /// This concrete class is used to mock/fake the weather data reader dependency for testing purposes.
    /// </summary>
    internal class MockWeatherDataReader : IWeatherDataReader
    {
        public List<WeatherData> Read(string path)
        {
            return new List<WeatherData>() { new WeatherData() { Date = DateTime.Today, Temperature = 10, Humidity = 50, Pressure = 1000 } };
        }
    }
}
