﻿using CsvHelper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.Start
{
    /// <summary>
    /// Class responsible for storing and transmitting weather data. <para></para>
    /// This data is read from different sources and logged in different ways. <para></para>
    /// These extra capabilities shouldnt be the responsibility of the WeatherStation itself, it should defer this to other classes. <para></para>
    /// How can this be refactored to meet SOLID?
    /// </summary>
    public class WeatherStation
    {
        // State
        public List<WeatherData> Readings { get; private set; } = new List<WeatherData>();
        public string Name { get; }

        public WeatherStation(string name)
        {
            Name = name;
        }

        // Behaviours, these should not be implemented by this class.
        // As this class is onlt responsible for storing and transmition,
        // this is achieved with the Readings property.
        // Dont worry about how this is done, we cover it in a later lesson.

        public void ReadWeatherData(string path)
        {
            if (!File.Exists(path))
                throw new FileNotFoundException();

            using var reader = new StreamReader(path);
            using var csv = new CsvReader(reader, CultureInfo.InvariantCulture);
            Readings = csv.GetRecords<WeatherData>().ToList();
        }

        public void LogWeatherData(WeatherData point)
        {
            Console.WriteLine("\nWEATHER LOG ---------------");
            Console.WriteLine($"Date: {point.Date}");
            Console.WriteLine($"Temperature: {point.Temperature}");
            Console.WriteLine($"Humidity: {point.Humidity}");
            Console.WriteLine($"Pressure: {point.Pressure}");
            Console.WriteLine("---------------------------\n");
        }
    }
}
