﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.LSP
{
    internal class RemoteWeatherStation : WeatherStation
    {
        public double Latitude { get; }
        public double Longitude { get; }

        public RemoteWeatherStation(string name, double latitude, double longitude) : base(name)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        // Instead of overriding, we make a new method.
        public void ReadWeatherDataRemote(string path)
        {
            Console.WriteLine("Connecting to remote server...");
            base.ReadWeatherData(path);
            Console.WriteLine("Reading complete");
        }
    }
}
