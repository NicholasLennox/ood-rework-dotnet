﻿using _2023ReworkOODLesson.SRP;
using _2023ReworkOODLesson.Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.LSP
{
    internal abstract class WeatherStation
    {
        public List<WeatherData> Readings { get; protected set; } = new List<WeatherData>();
        public string Name { get; }

        private readonly WeatherCSVReader _reader = new WeatherCSVReader();
        private readonly WeatherConsoleLogger _logger = new WeatherConsoleLogger();

        public WeatherStation(string name)
        {
            Name = name;
        }

        public virtual void ReadWeatherData(string path)
        {
            Readings = _reader.Read(path);
        }

        public virtual void LogWeatherData(WeatherData point)
        {
            _logger.Log(point);
        }
    }
}
