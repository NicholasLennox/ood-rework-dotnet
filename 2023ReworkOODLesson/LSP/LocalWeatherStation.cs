﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.LSP
{
    internal class LocalWeatherStation : WeatherStation
    {
        public string Address { get; }

        public LocalWeatherStation(string name, string address) : base(name)
        {
            Address = address;
        }
    }
}
