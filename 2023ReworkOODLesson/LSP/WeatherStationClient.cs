﻿using _2023ReworkOODLesson.OCP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.LSP
{
    /// <summary>
    /// Class responsible for using user input to create weather stations.
    /// For the sake of simplicity, most validation is ignored, as the focus is on LSP.
    /// </summary>
    internal class WeatherStationClient
    {
        // Using polymorphism we can have any type of station, as they are extendable.
        private WeatherStation? _station;

        public void SetupStation()
        {
            Console.WriteLine("What is the name of the station?: ");
            var name = Console.ReadLine();
            Console.WriteLine("Is this a (l)ocal or (r)emote station?: ");
            var stationType = Console.ReadLine();
            if(stationType == "l")
            {
                Console.WriteLine("What is the address?: ");
                var address = Console.ReadLine();
                _station = new LocalWeatherStation(name,address);
            } else if (stationType == "r") 
            {
                // For the sake of simplicity, we are going to leave out input validation.
                // It would be null and conversion checks to repropmt the user if failed.
                Console.WriteLine("What is the latitude?: ");
                var latitude = double.Parse(Console.ReadLine());
                Console.WriteLine("What is the longitude?: ");
                var longitude = double.Parse(Console.ReadLine());
                _station = new RemoteWeatherStation(name, latitude, longitude);
            } else
            {
                Console.WriteLine("Incorrect choice, try again");
                SetupStation();
            }
        }

        public void ReadData(string path)
        {
            // Here we call the base functionality. The local weather station will work fine.
            // However, remote will not work as intended, you wont get the "connecting..." written to the console.
            // This is because its a separate method called ReadWeatherDataRemote, so this calls the base.ReadWeatherData()
            // This means, RemoteWeatherStation fails LSP because it cant be substituted with its parent.
            _station.ReadWeatherData(path);
        }
    }
}
