﻿using _2023ReworkOODLesson.SRP;
using _2023ReworkOODLesson.Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.OCP.Interfaces
{
    internal class LocalWeatherStation : IWeatherStation
    {
        public string Address { get; }

        // Linking our private fields to their public properties.
        // This lets us control access while adhearing to the interface requirements.
        private string _name;
        public string Name => _name;

        private List<WeatherData> _readings = new List<WeatherData>();
        public List<WeatherData> Reading => _readings;

        // Declaring our dependencies
        private readonly WeatherCSVReader _reader = new WeatherCSVReader();
        private readonly WeatherConsoleLogger _logger = new WeatherConsoleLogger();

        public LocalWeatherStation(string name, string address)
        {
            _name = name;
            Address = address;
        }

        public void LogWeatherData(WeatherData point)
        {
            _logger.Log(point);
        }

        public void ReadWeatherData(string path)
        {
            // We cant map to the property directly, as it has no set.
            // So we need to directly mutate the underlying field.
            // This is fine to do in the class the field exists, but shouldnt be possible from outside.s
            _readings = _reader.Read(path);
        }
    }
}
