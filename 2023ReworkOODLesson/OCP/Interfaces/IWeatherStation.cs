﻿using _2023ReworkOODLesson.Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.OCP.Interfaces
{
    /// <summary>
    /// Alternative to abstract classes. It has benefits, but has the downside 
    /// of having restricted abilities to provide default implementations, 
    /// so you need to repeat things in concrete classes. <para></para>
    /// Its not to same as DRY repeating though, that is important to note.
    /// </summary>
    internal interface IWeatherStation
    {
        // We cant have a private/protected set, so to restrict access we need to manually
        // do the field mapping in the implementation.
        List<WeatherData> Reading { get; }
        string Name { get; }
        void ReadWeatherData(string path);
        void LogWeatherData(WeatherData point);
    }
}
