﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.OCP
{
    internal class RemoteWeatherStation : WeatherStation
    {
        // Extended state
        public double Latitude { get; }
        public double Longitude { get; }

        public RemoteWeatherStation(string name, double latitude, double longitude) : base(name)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        // Here we override reading data as we need to connect remotely
        public override void ReadWeatherData(string path)
        {
            Console.WriteLine("Connecting to remote server...");
            base.ReadWeatherData(path);
            Console.WriteLine("Reading complete");
        }
    }
}
