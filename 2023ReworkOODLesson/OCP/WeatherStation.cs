﻿using _2023ReworkOODLesson.SRP;
using _2023ReworkOODLesson.Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.OCP
{
    /// <summary>
    /// Provides an extendable base for WeatherStations
    /// </summary>
    internal abstract class WeatherStation
    {
        // We changed set to protected so it can be changed in the children.
        public List<WeatherData> Readings { get; protected set; } = new List<WeatherData>();
        public string Name { get; }

        private readonly WeatherCSVReader _reader = new WeatherCSVReader();
        private readonly WeatherConsoleLogger _logger = new WeatherConsoleLogger();

        public WeatherStation(string name)
        {
            Name = name;
        }

        // We change our behaviours to be virtual so they can be overridden by children.
        // This is only done because we expect it to be altered, it shouldnt just be a default.
        // If you know certain methods shouldnt be changed you dont include virtual.
        // You can also stop extension with the sealed keyword "public sealed class".
        public virtual void ReadWeatherData(string path)
        {
            Readings = _reader.Read(path);
        }

        public virtual void LogWeatherData(WeatherData point)
        {
            _logger.Log(point);
        }
    }
}
