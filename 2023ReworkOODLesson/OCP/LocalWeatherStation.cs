﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.OCP
{
    internal class LocalWeatherStation : WeatherStation
    {
        // Extended state
        public string Address { get; }

        public LocalWeatherStation(string name, string address) : base(name)
        {
            Address = address;
        }

        // This class doesnt change the way data is read or logged.
    }
}
