﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.ISP.InheritenceAndISP.Start
{
    /// <summary>
    /// <para>We start of with an interface to encapsulate payments. We begin with bank-related ones.</para>
    /// <para>This goes fine for our BankPayment class, however, when we extend the funcitonality to include loans, problems arise.</para>
    /// <para>Bank is forced to implement methods it cannot, same for LoanPayemnt. Why has this happened?</para>
    /// <para>Simple, we have broken both SRP and OCP for our interfaces (which is what ISP basically is). We came back and modified our IPayment instead of extending it, and we forced responsibility onto our classes which they are not meant to do.</para>
    /// </summary>
    internal interface IPayment
    {
        // Start
        List<object> GetPreviousPayments();
        void InitializePayments();
        object GetStatus();
        // Extended - will break existing impementations
        void InitiateLoadSettlement();
        void InitiateRepayments();

    }
}
