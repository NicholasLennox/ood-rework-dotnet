﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.ISP.InheritenceAndISP.Start
{
    internal class BankPayment : IPayment
    {
        public List<object> GetPreviousPayments()
        {
            return new List<object>() { new { Amount = 1000 } };
        }

        public object GetStatus()
        {
            return new { Status = "Owe 1000"};
        }

        public void InitializePayments()
        {
            Console.WriteLine("Sending invoice");
        }

        // We have no way of doing this.
        public void InitiateLoadSettlement()
        {
            throw new NotImplementedException();
        }

        public void InitiateRepayments()
        {
            throw new NotImplementedException();
        }
    }
}
