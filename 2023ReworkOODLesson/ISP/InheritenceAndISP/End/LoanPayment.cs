﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.ISP.InheritenceAndISP.End
{
    internal class LoanPayment : ILoanPayment
    {
        public List<object> GetPreviousPayments()
        {
            return new List<object> { };
        }

        public object GetStatus()
        {
            return new { Status = "6 months behind" };
        }

        public void InitiateLoadSettlement()
        {
            Console.WriteLine("Settlement agreement sent");
        }

        public void InitiateRepayments()
        {
            Console.WriteLine("Need to start paying");
        }
    }
}
