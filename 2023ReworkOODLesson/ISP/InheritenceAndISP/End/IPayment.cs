﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.ISP.InheritenceAndISP.End
{
    /// <summary>
    /// Parent payment interface to encapsulate all funcitonality all payment will have.
    /// </summary>
    internal interface IPayment
    {
        List<object> GetPreviousPayments();
        object GetStatus();
    }
}
