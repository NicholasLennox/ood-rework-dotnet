﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.ISP.InheritenceAndISP.End
{
    /// <summary>
    /// Interface to encapsulate bank-specific methods and include the common methods.
    /// </summary>
    internal interface IBankPayment : IPayment
    {
        void InitializePayments();
    }
}
