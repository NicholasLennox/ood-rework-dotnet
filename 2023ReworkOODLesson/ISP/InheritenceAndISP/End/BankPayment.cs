﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.ISP.InheritenceAndISP.End
{
    internal class BankPayment : IBankPayment
    {
        public List<object> GetPreviousPayments()
        {
            return new List<object>() { new { Amount = 1000 } };
        }

        public object GetStatus()
        {
            return new { Status = "Owe 1000" };
        }

        public void InitializePayments()
        {
            Console.WriteLine("Sending invoice");
        }
    }
}
