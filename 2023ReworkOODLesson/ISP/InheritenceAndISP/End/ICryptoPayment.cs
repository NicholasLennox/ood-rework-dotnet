﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.ISP.InheritenceAndISP.End
{
    /// <summary>
    /// New interface to show how easy payments are to extend now.
    /// </summary>
    internal interface ICryptoPayment : IPayment
    {
        object GetWallet();
        void TransferCoin();
    }
}
