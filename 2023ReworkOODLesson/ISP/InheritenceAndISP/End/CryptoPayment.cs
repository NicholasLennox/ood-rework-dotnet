﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.ISP.InheritenceAndISP.End
{
    /// <summary>
    /// Leaving out implementation, just used to show extension.
    /// </summary>
    internal class CryptoPayment : ICryptoPayment
    {
        public List<object> GetPreviousPayments()
        {
            throw new NotImplementedException();
        }

        public object GetStatus()
        {
            throw new NotImplementedException();
        }

        public object GetWallet()
        {
            throw new NotImplementedException();
        }

        public void TransferCoin()
        {
            throw new NotImplementedException();
        }
    }
}
