﻿using _2023ReworkOODLesson.Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.ISP.End
{
    /// <summary>
    /// Here we create an interface to encapsulate only logging. <para></para>
    /// This allows us to have different kinds of loggers to use and not be stuck to the console logger; and it satisfies ISP.
    /// </summary>
    internal interface IWeatherDataLogger
    {
        void Log(WeatherData data);
    }
}
