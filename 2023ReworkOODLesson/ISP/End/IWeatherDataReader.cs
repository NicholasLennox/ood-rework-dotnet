﻿using _2023ReworkOODLesson.Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.ISP.End
{
    /// <summary>
    /// Here we create an interface to encapsulate only reading. <para></para>
    /// This allows us to have different kinds of readers to use and not be stuck to the csv reader; and it satisfies ISP.
    /// </summary>
    internal interface IWeatherDataReader
    {
        List<WeatherData> Read(string path);
    }
}
