﻿using _2023ReworkOODLesson.Start;
using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.ISP.End.Readers
{
    internal class WeatherDataCSVReader : IWeatherDataReader
    {
        public List<WeatherData> Read(string path)
        {
            using var reader = new StreamReader(path);
            using var csv = new CsvReader(reader, CultureInfo.InvariantCulture);
            return csv.GetRecords<WeatherData>().ToList();
        }
    }
}
