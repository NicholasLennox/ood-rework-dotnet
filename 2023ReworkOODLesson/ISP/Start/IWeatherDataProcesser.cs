﻿using _2023ReworkOODLesson.Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.ISP.Start
{
    /// <summary>
    /// We want to make an interface to encapsulate the processing capabilities of our concrete classes. 
    /// <para>The idea behind this is to add more flexibility to our WeatherStation. Before this change, we were stuck using CSV readers and console loggers. What if we wanted to have different ways of reading or logging? Many changes would need to happen to our classes. This way, we can extend different types of processors and be able to simply change the implementation in the WeatherStation class.</para>
    /// <para>Although, this is a step in the right direction, making a single interface to encapsulate all our processing capabilities introduces new problems.</para>
    /// </summary>
    internal interface IWeatherDataProcesser
    {
        List<WeatherData> Read(string path);
        void Log(WeatherData data);
    }
}
