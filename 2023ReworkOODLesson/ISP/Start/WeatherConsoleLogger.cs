﻿using _2023ReworkOODLesson.Start;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.ISP.Start
{
    /// <summary>
    /// Here we have an implementaiton of our weather data processer interface. This class does logging well, but has no way to read data.
    /// </summary>
    internal class WeatherConsoleLogger : IWeatherDataProcesser
    {
        public void Log(WeatherData data)
        {
            Console.WriteLine("\nWEATHER LOG ---------------");
            Console.WriteLine($"Date: {data.Date}");
            Console.WriteLine($"Temperature: {data.Temperature}");
            Console.WriteLine($"Humidity: {data.Humidity}");
            Console.WriteLine($"Pressure: {data.Pressure}");
            Console.WriteLine("---------------------------\n");
        }

        public List<WeatherData> Read(string path)
        {
            throw new NotImplementedException();
        }
    }
}
