﻿using _2023ReworkOODLesson.Start;
using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.ISP.Start
{
    /// <summary>
    /// <para>Here we have our one implementation of a processor, our CSV reader.</para>
    /// <para>The read method is perfectly suited for this concrete class, however, our class does not Log.</para>
    /// </summary>
    internal class WeatherCSVReader : IWeatherDataProcesser
    {
        public void Log(WeatherData data)
        {
            throw new NotImplementedException();
        }

        public List<WeatherData> Read(string path)
        {
            using var reader = new StreamReader(path);
            using var csv = new CsvReader(reader, CultureInfo.InvariantCulture);
            return csv.GetRecords<WeatherData>().ToList();
        }
    }
}
