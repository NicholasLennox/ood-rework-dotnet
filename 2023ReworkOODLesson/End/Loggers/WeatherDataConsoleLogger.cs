﻿using _2023ReworkOODLesson.End.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.End.Loggers
{
    public class WeatherDataConsoleLogger : IWeatherDataLogger
    {
        public void Log(WeatherData data)
        {
            Console.WriteLine("\nWEATHER LOG ---------------");
            Console.WriteLine($"Date: {data.Date}");
            Console.WriteLine($"Temperature: {data.Temperature}");
            Console.WriteLine($"Humidity: {data.Humidity}");
            Console.WriteLine($"Pressure: {data.Pressure}");
            Console.WriteLine("---------------------------\n");
        }
    }
}
