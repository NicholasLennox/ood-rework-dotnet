﻿using _2023ReworkOODLesson.End.Loggers;
using _2023ReworkOODLesson.End.Readers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.End.Stations
{
    public class LocalWeatherStation : WeatherStation
    {
        public string Address { get; }

        public LocalWeatherStation(string name, 
            string address, 
            IWeatherDataReader reader, 
            IWeatherDataLogger logger) : base(name, reader, logger)
        {
            Address = address;
        }
    }
}
