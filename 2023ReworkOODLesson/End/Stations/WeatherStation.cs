﻿using _2023ReworkOODLesson.End.Data;
using _2023ReworkOODLesson.End.Loggers;
using _2023ReworkOODLesson.End.Readers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.End.Stations
{
    public abstract class WeatherStation
    {
        public List<WeatherData> Readings { get; protected set; } = new List<WeatherData>();
        public string Name { get; }

        // Kept it private by choice, want to force the children to call base. We can also change it to protected.
        private readonly IWeatherDataReader _reader;
        private readonly IWeatherDataLogger _logger;

  
        public WeatherStation(string name, IWeatherDataReader reader, IWeatherDataLogger logger)
        {
            Name = name;
            _reader = reader;
            _logger = logger;
        }

        public virtual void ReadWeatherData(string path)
        {
            Readings = _reader.Read(path);
        }

        public virtual void LogWeatherData(WeatherData point)
        {
            _logger.Log(point);
        }
    }
}
