﻿using _2023ReworkOODLesson.End.Loggers;
using _2023ReworkOODLesson.End.Readers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.End.Stations
{
    public class RemoteWeatherStation : WeatherStation
    {
        public double Latitude { get; }
        public double Longitude { get; }

        public RemoteWeatherStation(string name, 
            double latitude, 
            double longitude,
            IWeatherDataReader reader,
            IWeatherDataLogger logger) : base(name, reader, logger)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        public override void ReadWeatherData(string path)
        {
            Console.WriteLine("Connecting to remote server...");
            base.ReadWeatherData(path);
            Console.WriteLine("Reading complete");
        }
    }
}
