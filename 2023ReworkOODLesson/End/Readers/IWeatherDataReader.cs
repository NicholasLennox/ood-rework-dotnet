﻿using _2023ReworkOODLesson.End.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.End.Readers
{
    public interface IWeatherDataReader
    {
        List<WeatherData> Read(string path);
    }
}
