﻿using _2023ReworkOODLesson.End.Data;
using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.End.Readers
{
    internal class WeatherDataCSVReader : IWeatherDataReader
    {
        public List<WeatherData> Read(string path)
        {
            if (!File.Exists(path))
                throw new FileNotFoundException();

            using var reader = new StreamReader(path);
            using var csv = new CsvReader(reader, CultureInfo.InvariantCulture);
            return csv.GetRecords<WeatherData>().ToList();
        }
    }
}
