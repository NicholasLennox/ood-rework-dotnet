﻿using _2023ReworkOODLesson.ISP.End;
using _2023ReworkOODLesson.Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.DIP
{
    internal class WeatherStation
    {
        public List<WeatherData> Readings { get; protected set; } = new List<WeatherData>();
        public string Name { get; }

        // Changed to depend on an abstraction
        // We also removed constructors of dependencies (tight coupling)
        // We also have the flexibility to have different readers and loggers.
        private readonly IWeatherDataReader _reader;
        private readonly IWeatherDataLogger _logger;

        // We ask for the implementations of the dependenices in the constructor.
        // This pattern is called dependency injection. There is a class responsible for creating the instances as to avoid the coupling here.
        public WeatherStation(string name, IWeatherDataReader reader, IWeatherDataLogger logger)
        {
            Name = name;
            _reader = reader;
            _logger = logger;
        }

        public virtual void ReadWeatherData(string path)
        {
            Readings = _reader.Read(path);
        }

        public virtual void LogWeatherData(WeatherData point)
        {
            _logger.Log(point);
        }
    }
}
