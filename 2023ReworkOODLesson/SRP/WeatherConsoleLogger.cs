﻿using _2023ReworkOODLesson.Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.SRP
{
    internal class WeatherConsoleLogger
    {
        // Weather and Console are already implied with the class name, so we can just call this log.
        public void Log(WeatherData point)
        {
            Console.WriteLine("\nWEATHER LOG ---------------");
            Console.WriteLine($"Date: {point.Date}");
            Console.WriteLine($"Temperature: {point.Temperature}");
            Console.WriteLine($"Humidity: {point.Humidity}");
            Console.WriteLine($"Pressure: {point.Pressure}");
            Console.WriteLine("---------------------------\n");
        }
    }
}
