﻿using _2023ReworkOODLesson.Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.SRP
{
    // We now have classes to encapsulate the responsibility of our  reading and logging.
    internal class WeatherStation
    {
        public List<WeatherData> Readings { get; private set; } = new List<WeatherData>();
        public string Name { get; }

        // They dont need to be properties, because they are purely functional and require no underlying state to protect.
        // This is also setting us up for a later principle.
        private readonly WeatherCSVReader _reader = new WeatherCSVReader();
        private readonly WeatherConsoleLogger _logger = new WeatherConsoleLogger();

        public WeatherStation(string name)
        {
            Name = name;
        }

        // We keep the interface (how classes acces this classes functionality) the same
        // as to not break any clients (Program.cs for example).
        // Our functionality that was not our responsibility is not deffered and can be tested independently.
        public void ReadWeatherData(string path)
        {
            Readings = _reader.Read(path);
        }

        public void LogWeatherData(WeatherData point)
        {
            _logger.Log(point);
        }
    }
}
