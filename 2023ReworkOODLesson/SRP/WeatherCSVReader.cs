﻿using _2023ReworkOODLesson.Start;
using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2023ReworkOODLesson.SRP
{
    /// <summary>
    /// This class encapsulates the responsibility of reading weather data from a CSV source.
    /// </summary>
    internal class WeatherCSVReader
    {
        // We dont need to include Weather or CSV in the name for this method,
        // as its already implied with the class name.
        public List<WeatherData> Read(string path)
        {
            using var reader = new StreamReader(path);
            using var csv = new CsvReader(reader, CultureInfo.InvariantCulture);
            return csv.GetRecords<WeatherData>().ToList();
        }
    }
}
